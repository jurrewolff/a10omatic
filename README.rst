a10omatic
=========
The a10omatic allows for creating pre-configured partitions on A10 Load Balancers in a consistent and reliable manner. The a10omatic as a product is the result of an internship assignment at Cyso, realized by Jurre Wolff.

Description
-----------
The a10omatic is has been developed to minimize the time required to create and pre-configure partitions on A10 Load Balancers. This task takes time and is very repetitive. The a10omatic speeds up this process by automatically creating and configuring a partition within seconds, based on a user defined configuration.

It must be made clear that the a10omatic is meant to be used **only** for creating working base partitions. This base partition represents the absolute minimum configuration required for a partition at Cyso to be functional. This means that the a10omatic creates working partitions ready to be built upon for adding customer specific needs.

The a10omatic is **not** designed as a tool for adding functionality to already existing partitions.

Usage
-----
The easiest way of using the a10omatic is by running it using the following command::

    a10omatic --yaml-file path/to/config_file.yaml

*>Note: The config-file must at least contain every required variable before the a10omatic will work. The example template shown in the next section contains all of these variables and its contents can be used for your own custom yaml config files.*

To get a description of all variables required by the a10omatic you can consult the following command::

    a10omatic -h

This will print all arguments supported by the a10omatic, as well as information about each argument. This is useful if you are unsure about what certain variables mean or do.


    *>Note: After the a10omatic has done its job it is essential to check the configuration on the A10-device manually. (Using CLI or Web-Interface)*

Another way of configuring the a10omatic is by passing the results of the -h option within CLI. Additionally environment variables can be used.

Config File: Example template
-----------------------------
A working template for your yaml configuration file is shown below. This template can be modified to suite the needs of your particular partition. The template contains more information about some parameters:
::

    # Settings below define application behavior
    configfile:
      logger:
        cfgpath: null # Used to set custom logger config path (Yaml). Set to 'null' or leave blank for default config
      ssl:
        cfgpath: 'default' # Used to set custom ssl-template config path(Yaml). Set to 'default' for default config.
    
    warning:
      insecurerequest: False   # Enable/Disable insecurerequest warnings
    
    onfail:
      deletepart: True        # Enable/Disable partition removal when creation fails halfway through
    
    # Settings for connecting to A10-Device
    login:
      endpoint: 'https://admin:password@lb2-ams-vcs.network.cyso.net:443/'   # Format: https://username:password@host:port/
    
    # Settings below configure partition to be created by a10omatic
    partition:
      name: 'Customer_Name'  # Insert customer name who will use the partition
      apptype: 'adc'
    
    pubvlan:          # Fill the blanks with relevant values
      name: 'vl10XX'
      id: '10XX'
      ve:
        ip1: 'x.y.z.a'  # IP for device context 1
        ip2: 'x.y.z.b'  # IP for device context 2
        mask: '/zz'
      taggedeths:
        - 5
    
    intvlan:
      name: 'vl36XX'
      id: '36XX'
      ve:
        ip1: '10.36.xx.a'
        ip2: '10.36.xx.b'
        mask: '/zz'
      taggedeths:
        - 6
    
    defaultroute:   # Are created on both device-contexts but are identical cfg wise
      address: '0.0.0.0'
      mask: '/0'
      gateway: 'gateway_address'  # Insert relevant gateway address
    
    natpool:  # Fill in the blanks with relevant values
      name: 'vl36XX-NAT-pool'
      ipstart: '10.36.XX.240'
      ipend: '10.36.XX.244'
      mask: '/24'
      iprr: True
      vrid: 2
    
    vrrp:
      vridlist:   # List of VRIDs to be created --> 0 does not work.
        - 1
        - 2
      priority1: 160    # Priority of VRIDs in Device-Context 1
      priority2: 200    # Priority of VRIDs in Device-Context 2

    ascii: False  # Print something nice

As the example yaml file above suggests, the a10omatic logger and SSL-Template functionalities can optionally be configured using yaml files. Example configurations for these are shown below:

**SSL-Template yaml config example:**
::

    cipherName: 'CipherName'
    cipherCfg:    # Entries can be added/deleted/modified
      - cipher-suite: TLS1_RSA_AES_128_SHA
        priority: 
      - cipher-suite: TLS1_RSA_AES_256_SHA 
        priority: 
      - cipher-suite: TLS1_ECDHE_RSA_AES_128_SHA
        priority: 5

More values can be added to the cipherCfg key. It's important that for each extra value a cipher-suite and a corresponding priority are appended in the form of a list. For the use of default priorities fields can be left blank.

**Logger yaml config example:**
::

    disable_existing_loggers: False
    
    version: 1
    
    formatters:
      short:
        format: '%(asctime)s : %(levelname)s: %(name)s: %(message)s'
      shorter:
        format: '%(levelname)s: %(name)s: %(message)s'
    
    handlers:
      console:
        level: INFO
        formatter: shorter
        class: logging.StreamHandler
      file:
        level: DEBUG
        class: logging.FileHandler
        formatter: short
        filename: action.log
    
    loggers:
      '':
        handlers:
          - console
          - file
        level: WARNING
      __main__:
        level: DEBUG
        propagate: True
      a10omatic:
        level: DEBUG
        propagate: True
      urllib3:
        level: DEBUG
        propagate: True


License
-------
Proprietary, all rights reserved, do not release.