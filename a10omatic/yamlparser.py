import yaml
from a10omatic.exceptions import YAMLLoadError


def load_yaml(filename):
    """Class contains config file in dict format to be used for future requests."""
    try:
        with open(filename) as f:
            return yaml.safe_load(f)
    except Exception as ex:
        raise YAMLLoadError('Loading config failed: {}'.format(ex))
