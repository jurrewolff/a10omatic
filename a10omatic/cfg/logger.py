def return_dict_logger_config():

    dictLoggerConfig = {'disable_existing_loggers': False,
                        'formatters': {'short': {'format': '%(asctime)s : %(levelname)s: %(name)s: %(message)s'},
                                       'shorter': {'format': '%(levelname)s: %(name)s: %(message)s'}},
                        'handlers': {'console': {'class': 'logging.StreamHandler',
                                                 'formatter': 'shorter',
                                                 'level': 'INFO'},
                                     'file': {'class': 'logging.FileHandler',
                                              'filename': 'action.log',
                                              'formatter': 'short',
                                              'level': 'DEBUG'}},
                        'loggers': {'': {'handlers': ['console', 'file'], 'level': 'WARNING'},
                                    '__main__': {'level': 'DEBUG', 'propagate': True},
                                    'a10omatic': {'level': 'DEBUG', 'propagate': True},
                                    'urllib3': {'level': 'ERROR', 'propagate': True}},
                        'version': 1}

    return dictLoggerConfig
