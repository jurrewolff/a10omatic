def return_cipher():

    cipher = {'cipherName': 'CipherNaam',
              'cipherTemplate': [

                  {'cipher-suite': 'TLS1_RSA_AES_128_SHA',
                   'priority': None},
                  {'cipher-suite': 'TLS1_RSA_AES_256_SHA',
                   'priority': None},
                  {'cipher-suite': 'TLS1_ECDHE_RSA_AES_128_SHA',
                   'priority': 5},
                  {'cipher-suite': 'TLS1_ECDHE_RSA_AES_256_SHA',
                   'priority': 5},
                  {'cipher-suite': 'TLS1_ECDHE_ECDSA_AES_128_SHA',
                   'priority': None},
                  {'cipher-suite': 'TLS1_ECDHE_ECDSA_AES_256_SHA',
                   'priority': 5},
                  {'cipher-suite': 'TLS1_RSA_AES_128_SHA256',
                   'priority': None},
                  {'cipher-suite': 'TLS1_RSA_AES_256_SHA256',
                   'priority': None},
                  {'cipher-suite': 'TLS1_ECDHE_RSA_AES_128_SHA256',
                   'priority': 5},
                  {'cipher-suite': 'TLS1_ECDHE_RSA_AES_256_GCM_SHA384',
                   'priority': 5},
                  {'cipher-suite': 'TLS1_ECDHE_ECDSA_AES_128_SHA256',
                   'priority': 5},
                  {'cipher-suite': 'TLS1_ECDHE_ECDSA_AES_256_GCM_SHA384',
                   'priority': 5},
              ]
              }

    return cipher
