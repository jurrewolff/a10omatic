from omniconf import config
import sys
import os
import logging
from a10omatic.cfg.cipher import return_cipher

import a10omatic.actions as actions
import a10omatic.yamlparser
from a10omatic.config import load_config


def delete_after_fail(client):
    logger = logging.getLogger(__name__)

    # If partition already exists, it should not be deleted
    if config('onfail.deletepart'):
        logger.warning('Deleting partition: {}'.format(
            config('partition.name')))
        actions.Partition(client).delete_part(config('partition.name'))
        sys.exit(1)

    logger.warning('Failed partition {} will NOT be deleted, as user configured not to.'.format(
        config('partition.name')))
    sys.exit(1)


def main():
    actions.configure_logger()      # First load logger with application's default config
    logger = logging.getLogger(__name__)

    try:
        load_config()
    except Exception as ex:
        logger.error(
            'An error occured while loading configuration: {}'.format(ex))
        sys.exit(1)

    if config('ascii'):
        print(actions.a10omatic_ascii())

    if config('configfile.logger.cfgpath'):     # Then give option for custom logger cfg
        actions.configure_logger(config('configfile.logger.cfgpath'))

    if not config('warning.insecurerequest'):
        actions.disable_warning()

    if config('certificate.path'):      # Very nice, but useless :)
        logger.info('Using certificate: {}'.format(config('certificate.path')))
        os.environ['REQUESTS_CA_BUNDLE'] = config('certificate.path')

    try:
        client = actions.ActionsClient(config('login.endpoint'))
    except BaseException as ex:
        raise logger.error('Parsing of provided endpoint failed: {}'.format(ex))

    # Check for partition existence
    try:
        if actions.Partition(client).check_part(config('partition.name')):
            logging.warning('Partition {} has already been created earlier. A10omatic will not proceed.'.format(
                config('partition.name')))
            os._exit(1)

    except BaseException as ex:
        logger.error('Connection or certificate verification failed: {}'.format(ex))
        sys.exit(1)

    try:
        # Create partition
        actions.Partition(client).create_part(
            config('partition.name'),
            config('partition.apptype')
        )

        # Activate partition
        client.active_partition(config('partition.name'))

        # Create public VLAN
        client.switch_context(1)        # Context 1

        actions.VLAN(client).create_vlan(
            config('pubvlan.name'),
            config('pubvlan.id'),
            config('pubvlan.taggedeths'),
            config('pubvlan.id')
        )

        actions.VLAN(client).create_ve(
            config('pubvlan.id'),
            config('pubvlan.ve.ip1'),
            config('pubvlan.ve.mask')
        )

        client.switch_context(2)        # Context 2

        actions.VLAN(client).create_vlan(
            config('pubvlan.name'),
            config('pubvlan.id'),
            config('pubvlan.taggedeths'),
            config('pubvlan.id')
        )

        actions.VLAN(client).create_ve(
            config('pubvlan.id'),
            config('pubvlan.ve.ip2'),
            config('pubvlan.ve.mask')
        )

        # Create internal VLAN
        client.switch_context(1)        # Context 1

        actions.VLAN(client).create_vlan(
            config('intvlan.name'),
            config('intvlan.id'),
            config('intvlan.taggedeths'),
            config('intvlan.id')
        )

        actions.VLAN(client).create_ve(
            config('intvlan.id'),
            config('intvlan.ve.ip1'),
            config('intvlan.ve.mask')
        )

        client.switch_context(2)        # Context 2

        actions.VLAN(client).create_vlan(
            config('intvlan.name'),
            config('intvlan.id'),
            config('intvlan.taggedeths'),
            config('intvlan.id')
        )

        actions.VLAN(client).create_ve(
            config('intvlan.id'),
            config('intvlan.ve.ip2'),
            config('intvlan.ve.mask')
        )

        # Create and configure VRIDs
        actions.VRRP(client).create_vrid(     # Create two vrids
            [config('vrrp.vridlist')[0], config('vrrp.vridlist')[1]]
        )

        client.switch_context(1)        # Context 1
        actions.VRRP(client).configure_vrid(      # Configure two vrids
            [config('vrrp.vridlist')[0], config('vrrp.vridlist')[1]],
            config('vrrp.priority1')
        )

        client.switch_context(2)        # Context 2
        actions.VRRP(client).configure_vrid(
            [config('vrrp.vridlist')[0], config('vrrp.vridlist')[1]],
            config('vrrp.priority2')
        )

        # Create NAT-pool
        actions.NatPool(client).create_nat_pool(
            config('natpool.name'),
            config('natpool.ipstart'),
            config('natpool.ipend'),
            config('natpool.mask'),
            config('natpool.iprr'),
            config('natpool.vrid')
        )

        # Set default routes
        client.switch_context(1)        # Context 1
        actions.DefaultRoute(client).create_default_route(
            config('defaultroute.address'),
            config('defaultroute.mask'),
            config('defaultroute.gateway')
        )

        client.switch_context(2)        # Context 2
        actions.DefaultRoute(client).create_default_route(
            config('defaultroute.address'),
            config('defaultroute.mask'),
            config('defaultroute.gateway')
        )

        # Create SSL-Template
        if config('configfile.ssl.cfgpath') != 'default':  # User defined path
            logger.info('Using user provided config for SSL-Template')
            yamlPath = config('configfile.ssl.cfgpath')
            cipher = a10omatic.yamlparser.load_yaml(yamlPath)

        else:  # Revert to default config
            logger.info('Using default config for SSL-Template')
            cipher = return_cipher()

            actions.SSL(client).create_ssl_template(
                cipher['cipherName'],
                cipher['cipherTemplate']
            )

        # HTTP template
        actions.HTTP(client).create_http_template(
            name='X-Forwarded-For',
            ins_clnt_ip=1,
            ins_clnt_ip_hdr_name='',        # Intentionally empty
            clnt_ip_hdr_replace=1,
        )

        # Persistent Cookie
        actions.PersistentCookie(client).create_persistent_cookie(
            name='CookiePersistence',
            domain=None
        )

        # Save changes
        client.write_mem('primary', 'all')
        client.write_mem('secondary', 'all')
        logger.info('A10omatic successfully executed. Review changes in CLI or GUI at host')

    except (RuntimeError, BaseException) as ex:
        logger.error("Unexpected error: {}".format(ex))
        try:
            delete_after_fail(client)
        except BaseException as ex:
            logger.error('Deleting partition failed: {}'.format(ex))
        client.kill_session()
        sys.exit(1)

    finally:
        # Close
        client.kill_session()


if __name__ == '__main__':
    """Runs program"""
    main()
