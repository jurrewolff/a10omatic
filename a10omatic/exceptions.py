

class ComponentDoesNotExist(RuntimeError):
    pass


class ComponentAlreadyExists(RuntimeError):
    pass


class AcosCreationError(RuntimeError):
    pass


class AcosContextSwitch(RuntimeError):
    pass


class YAMLLoadError(RuntimeError):
    pass


class AcosCheckingError(RuntimeError):
    pass
