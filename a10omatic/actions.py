from a10omatic.exceptions import (ComponentDoesNotExist, ComponentAlreadyExists, AcosCreationError, AcosContextSwitch,
                                  AcosCheckingError)
from urllib.parse import urlparse
from acos_client import errors as acos_errors
import acos_client as acos
import logging
import logging.config
import warnings
import a10omatic.yamlparser
from a10omatic.cfg.logger import return_dict_logger_config


def configure_logger(yamlLoggerConfig=None):
    """Configures and returns logger"""
    if yamlLoggerConfig:
        dictLoggerConfig = a10omatic.yamlparser.load_yaml(yamlLoggerConfig)
    else:
        dictLoggerConfig = return_dict_logger_config()

    logging.config.dictConfig(dictLoggerConfig)
    logger = logging.getLogger(__name__)
    logger.debug('Logging initialized')


def disable_warning():
    logger = logging.getLogger(__name__)
    logger.warning('SSL verification has been disabled!')
    warnings.simplefilter("ignore", Warning)


class ActionsClient:
    """
    Class can be used as wrapper containing a session for requests.
    Sessions can be requested, by default sessions are using the shared partition.
    Using the provided methods, the session behaviour can be altered.
    """

    def __init__(self, endpoint):
        self.logger = logging.getLogger(__name__)
        self.session = self.create_client(endpoint)

    def create_client(self, endpoint):
        """Method returns session, with the default 'shared' parition selected."""
        try:
            self.logger.info('Creating client')
            endpoint = urlparse(endpoint)

            return acos.Client(
                endpoint.hostname,
                acos.AXAPI_30,
                endpoint.username,
                endpoint.password,
                port=endpoint.port
            )
        # If exception is no ACOS-Exception, cli.py should take care of it.
        except acos_errors.ACOSException as ex:
            raise AcosCreationError('Client creation failed: {}'.format(ex))

    def active_partition(self, name):
        """Method returns session, but with specified partition acivated."""
        if not self.session.system.partition.exists(name):
            # If partition is non-existent, shared partition will be used.
            raise ComponentDoesNotExist('Cannot switch partition. Partition does not exist')

        self.session.system.partition.active(name)
        self.logger.info('Activated partition: {}'.format(name))

    def switch_context(self, deviceContext):
        try:
            self.session.device_context.switch(deviceContext, 0)
            self.logger.info('Switched to device-context: {}'.format(deviceContext))
        except acos_errors.ACOSException as ex:
            raise AcosContextSwitch('Failed switching to device-context {}: {}'.format(deviceContext, ex))

    def write_mem(self, destination, partition):
        """
        Method writes configuration to disk, making changes persistent.
        Note: If shared partition is not active, ACOS-client will, logoff and on again before writing to disk.
        """
        self.logger.info('Saving changes to {} configuration...'.format(destination))
        self.session.system.action.write_memory(destination, partition)

    def kill_session(self):
        """After all operations have been completed, the session can be shut-down using this method"""
        self.logger.debug('Shutting down session...')
        self.session.session.close()


class Partition:
    """Class contains methods for partition creation/checking."""

    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger(__name__)

    def create_part(self, name, appType):
        """
        Method creates partition.
        Only after method check_part() returns 'False' method will accept creation of new partition.
        """

        exists = self.check_part(name)
        if exists:
            raise ComponentAlreadyExists('Partition {} already exists'.format(name))

        try:
            self.client.session.system.partition.create(
                name, appType)     # Creates new partition
            self.logger.info('Created partition: {}'.format(name))
        except acos_errors.ACOSException as ex:
            raise AcosCreationError('Partition creation failed: {}'.format(ex))

    def delete_part(self, name):
        """
        Method deletes specified partition.
        Only after method check_part() returns 'False' method will accept deletion of partition.
        """

        exists = self.check_part(name)
        if not exists:
            raise ComponentDoesNotExist('Partition {} does not exist'.format(name))

        try:
            self.client.session.system.partition.delete(name)
            self.logger.warning('Deleted partition: {}'.format(name))
        except acos_errors.ACOSException as ex:
            raise AcosCreationError('Partition deletion failed: {}'.format(ex))

    def check_part(self, name):
        """Checking partition existence. Returns True when specified partition exists."""
        try:
            return self.client.session.system.partition.exists(name)
        except acos_errors.ACOSException as ex:
            raise AcosCheckingError('Checking partition failed: {}'.format(ex))


class DefaultRoute:
    """Class contains methods for deafault-route creation/checking."""

    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger(__name__)

    def create_default_route(self, drAddress, drMask, drGateway):
        """
        Configures the default-route for selected device-contexts.
        Code allows for per-device configuration.
        """

        exists = self.check_default_route(drAddress, drMask)
        if exists:
            raise ComponentAlreadyExists('Default-route with ip {} already exists'.format(drAddress))

        try:
            self.client.session.route.create(
                destination=drAddress,
                mask=drMask,
                next_hops=[[drGateway, 1]]      # Compose lst (Required by route creation)
            )
            self.logger.info('Created default route: {}'.format(drAddress))
        except acos_errors.ACOSException as ex:
            raise AcosCreationError('Default-Route creation failed: {}'.format(ex))

    def check_default_route(self, drAddress, drMask):
        """Checking default-route existence. Returns True when specified route exists."""
        try:
            return self.client.session.route.exists(drAddress, drMask)
        except acos_errors.ACOSException as ex:
            raise AcosCheckingError('Checking default route failed: {}'.format(ex))


class NatPool:
    """Class contains methods for Nat-Pool creation/checking."""

    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger(__name__)

    def create_nat_pool(self, natName, natStartIP, natEndIP, natMask, iprr, natVrid):
        """
        Method creates new NAT-Pool.
        Only after method check_nat_pool() returns 'False' method will accept creation of new pool.
        """

        exists = self.check_nat_pool(natName)
        if exists:
            raise ComponentAlreadyExists('NAT-Pool {} already exists'.format(natName))

        try:
            self.client.session.nat.pool.create(
                name=natName,
                start_ip=natStartIP,
                end_ip=natEndIP,
                mask=natMask,
                ip_rr=iprr,
                vrid=natVrid
            )
            self.logger.info('Created NAT-Pool: {}'.format(natName))
        except acos_errors.ACOSException as ex:
            if ex.code == 1023460352:
                raise AcosCreationError('Nat-Pool creation failed: VRID does not yet exist')
            raise AcosCreationError('Nat-Pool creation of {} failed: VRID {} \
                                    does not yet exist'.format(natName, natVrid))

    def check_nat_pool(self, natName):
        """Checking NAT-Pool existence. Returns True when specified NAT-Pool exists."""
        try:
            return self.client.session.nat.pool.exists(natName)
        except acos_errors.ACOSException as ex:
            raise AcosCheckingError('Checking NAT-Pool {} failed: {}'.format(ex, natName))


class VLAN:
    """
    Class contains methods for Vcheck_partAN and Virtual-Ethernet creation/checking.
    These two functions are combined in one class as they are directly related.
    """

    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger(__name__)

    def create_vlan(self, vlanName, vlanID, taggedEths, veID=None):
        """
        Method creates VLAN.
        Only after method check_vlan() returns 'False', method will accept creation of new VLAN.
        """

        exists = self.check_vlan(vlanID)
        if exists:
            raise ComponentAlreadyExists('VLAN {} already exists'.format(vlanName))

        try:
            self.client.session.vlan.create(
                name=vlanName,
                vlan_id=vlanID,
                tagged_eths=taggedEths,
                veth=True
            )
            self.logger.info('Created VLAN: {}'.format(vlanName))
        except acos_errors.ACOSException as ex:
            if ex.code == 721420551:
                raise AcosCreationError('VLAN creation failed: Device-Context does not exist')
            raise AcosCreationError('VLAN creation of {} failed: Device-Context does not exist'.format(vlanName))

    def check_vlan(self, vlanID):
        """Checking VLAN existence. Returns True when specified VLAN exists."""
        try:
            return self.client.session.vlan.exists(vlanID)
        except acos_errors.ACOSException as ex:
            raise AcosCheckingError('Checking VLAN failed: {}'.format(ex))

    def create_ve(self, veID, veIP, veMask):
        """Method updates existing virtual-ethernet interface."""

        try:
            self.client.session.interface.ve.update(
                ifnum=veID,
                ip_address=veIP,
                ip_netmask=veMask,
            )
            self.logger.info('Created/Updated Virtual-Ethernet with ID: {}'.format(veID))
        except acos_errors.ACOSException as ex:
            if ex.code == 1023460352:
                raise AcosCreationError('VE creation of {} failed: Create VLAN first'.format(veID))
            raise AcosCreationError('VE creation of {} failed: {}'.format(veID, ex))


class VRRP:
    """
    Class contains methods for VRRP configuration.
    """

    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger(__name__)

    def create_vrid(self, vridList):
        """
        Creates VRID, but only after check_vrid return False.
        Function has it's own method as it is seperate from the configuration process.
        """

        for vrid in vridList:
            exists = self.check_vrid(vrid)
            if exists:
                raise ComponentAlreadyExists('VRID {} already exists'.format(vrid))
            try:
                self.client.session.vrrpa.create(vrid_val=vrid)
                self.logger.info('Created VRID: {}'.format(vrid))
            except acos_errors.ACOSException as ex:
                raise AcosCreationError('VRID creation failed: {}'.format(ex))

    def check_vrid(self, vrid):
        """Checking VRID existence. Returns True when specified VRID exists."""
        try:
            return self.client.session.vrrpa.exists(vrid)
        except acos_errors.ACOSException as ex:
            raise AcosCheckingError('Checking failed: {}'.format(ex))

    def configure_vrid(self, vridList, priority):
        """
        Method configures previously created VRIDs.
        Only configures within the provided device-contexts and selected vrid.
        """

        for vrid in vridList:
            try:
                self.client.session.vrrpa.blade.create(
                    vrid_val=vrid, priority=priority)
                self.logger.info('Configured VRID: {}'.format(vrid))
            except acos_errors.ACOSException as ex:
                if ex.code == 1023460352:
                    raise ComponentDoesNotExist('VRID {} does not exists'.format(vrid))
                raise AcosCreationError('VRID configuration of {} failed: {}'.format(vrid, ex))


class SSL:
    """
    Class contains methods for SSL-Template creation.
    SSL-Template configuration is stored in cipher.yaml.
    """

    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger(__name__)

    def create_ssl_template(self, cipherName, cipherTemplate):
        """
        Method creates SSL-Template.
        Template is generated from cipherName (str) and cipherTemplate (dict).
        The dict cipherTemplate should contain two keys: 'cipher-suite' and 'priority'.
        """

        exists = self.check_ssl(cipherName)
        if exists:
            raise ComponentAlreadyExists('SSL-Template {} already exists'.format(cipherName))

        try:
            self.client.session.slb.template.cipher_ssl.create(
                name=cipherName,
                cipher_template=cipherTemplate
            )
            self.logger.info('Created SSL-Template: {}'.format(cipherName))
        except acos_errors.ACOSException as ex:
            raise AcosCreationError('SSL-Template creation of {} failed: {}'.format(cipherName, ex))

    def check_ssl(self, cipherName):
        """Checking SSL-Template existence. Returns True when specified Template exists."""
        try:
            return self.client.session.slb.template.cipher_ssl.exists(cipherName)
        except acos_errors.ACOSException as ex:
            raise AcosCheckingError('Checking failed: {}'.format(ex))


class HTTP:
    """Class contains methods for HTTP-Template creation/checking"""

    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger(__name__)

    def create_http_template(self, name, ins_clnt_ip=None, ins_clnt_ip_hdr_name=None,
                             clnt_ip_hdr_replace=None):
        """
        Method creates HTTP-Template.
        Only allows creation after check_http() method returns False.
        """
        exists = self.check_http(name)
        if exists:
            raise ComponentAlreadyExists('HTTP-Template {} already exists'.format(name))

        try:
            self.client.session.slb.template.http_template.create(
                name=name,
                insert_client_ip=ins_clnt_ip,
                insert_client_ip_header_name=ins_clnt_ip_hdr_name,
                client_ip_hdr_replace=clnt_ip_hdr_replace,
            )
            self.logger.info('Created HTTP-Template: {}'.format(name))
        except acos_errors.ACOSException as ex:
            raise AcosCreationError('HTTP-Template creation of {} failed: {}'.format(name, ex))

    def check_http(self, name):
        """Checking HTTP-Template existence. Returns True when specified Template exists."""
        try:
            return self.client.session.slb.template.http_template.exists(name)
        except acos_errors.ACOSException as ex:
            raise AcosCheckingError('Checking failed: {}'.format(ex))


class PersistentCookie:
    """Class contains methods for persistent cookie creation/checking."""

    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger(__name__)

    def create_persistent_cookie(self, name, domain):
        """
        Method creates persistent cookie entries.
        Only allows creation after check_cookie() method returns False.
        """
        exists = self.check_cookie(name)
        if exists:
            raise ComponentAlreadyExists('Persistent cookie {} already exists'.format(name))

        try:
            self.client.session.slb.template.cookie_persistence.create(name, domain)
            self.logger.info('Created Persistent cookie: {}'.format(name))
        except acos_errors.ACOSException as ex:
            raise AcosCreationError('Persistent cookie creation of {} failed: {}'.format(name, ex))

    def check_cookie(self, name):
        """Checking Persistent cookie existence. Returns True when specified cookie exists."""
        try:
            return self.client.session.slb.template.cookie_persistence.exists(name)
        except acos_errors.ACOSException as ex:
            raise AcosCheckingError('Checking failed: {}'.format(ex))


def a10omatic_ascii():
    a10omatic_ascii = r'''
             /$$    /$$$$$$                                      /$$     /$$
           /$$$$   /$$$_  $$                                    | $$    |__/
  /$$$$$$ |_  $$  | $$$$\ $$  /$$$$$$  /$$$$$$/$$$$   /$$$$$$  /$$$$$$   /$$  /$$$$$$$
 |____  $$  | $$  | $$ $$ $$ /$$__  $$| $$_  $$_  $$ |____  $$|_  $$_/  | $$ /$$_____/
  /$$$$$$$  | $$  | $$\ $$$$| $$  \ $$| $$ \ $$ \ $$  /$$$$$$$  | $$    | $$| $$
 /$$__  $$  | $$  | $$ \ $$$| $$  | $$| $$ | $$ | $$ /$$__  $$  | $$ /$$| $$| $$
|  $$$$$$$ /$$$$$$|  $$$$$$/|  $$$$$$/| $$ | $$ | $$|  $$$$$$$  |  $$$$/| $$|  $$$$$$$
 \_______/|______/ \______/  \______/ |__/ |__/ |__/ \_______/   \___/  |__/ \_______/ by Jurre Wolff
    '''
    return a10omatic_ascii
