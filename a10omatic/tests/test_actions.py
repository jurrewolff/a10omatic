# Copyright (c) 2018 Jurre Wolff <jurre.wolff@cyso.com>
#
# This file is part of a10omatic .
#
# All rights reserved.
import unittest
from unittest.mock import patch
from a10omatic.actions import Partition, DefaultRoute, NatPool, VLAN, VRRP, SSL, HTTP, PersistentCookie, ActionsClient
from a10omatic.exceptions import ComponentDoesNotExist, AcosCreationError, ComponentAlreadyExists, \
    AcosCheckingError

from acos_client import errors as acos_errors
from acos_client.v30.vlan import Vlan as mockVlan
from acos_client.v30.nat import Nat as mockNat
from acos_client.v30.route import RIB as mockRIB
from acos_client.v30.partition import Partition as mockPartition
from acos_client.v30.vrrpa.vrid import VRID as mockVRID
from acos_client.v30.vrrpa.vrid import BladeParameters as mockBP
from acos_client.v30.slb.template.ssl import SSLCipher as mockSSLCipher
from acos_client.v30.slb.template.l7 import HTTPTemplate as mockHTTPTemplate
from acos_client.v30.slb.template.persistence import CookiePersistence as mockCookiePersistence

import vcr
import pytest


endpoint = 'https://admin:a10@192.168.64.131:443/'


class TestPartition(unittest.TestCase):

    def setUp(self):
        self.client = ActionsClient(endpoint)
        with vcr.use_cassette('vcr/recordings/setup.yaml'):
            self.client.active_partition('partName')

        self.partition = Partition(self.client)

    def test_check_part_true(self):
        with vcr.use_cassette('vcr/recordings/partition/check_part_true.yaml'):
            assert self.partition.check_part('partName') is True  # Pytest provides more info

    def test_check_part_false(self):
        with vcr.use_cassette('vcr/recordings/partition/check_part_false.yaml'):
            assert self.partition.check_part('partNameNonExistent') is False

    @pytest.mark.xfail(raises=AcosCheckingError, strict=True)  # Expect failure
    def test_check_part_except(self):
        with patch.object(mockPartition, "exists", side_effect=acos_errors.ACOSException()):
            self.partition.check_part("partNameNonExistent")

    def test_create_part_exists(self):
        with vcr.use_cassette('vcr/recordings/partition/create_part_exists.yaml'):
            with pytest.raises(ComponentAlreadyExists):
                self.partition.create_part('partName', 'adc')

    def test_create_part_creation_fail(self):
        with vcr.use_cassette('vcr/recordings/partition/create_part_fail.yaml'):
            with pytest.raises(AcosCreationError):
                self.partition.create_part('partNameWaaaaaaaayTooLong', 'adc')  # Too long name triggers except

    def test_delete_part_non_existent(self):
        with vcr.use_cassette('vcr/recordings/partition/delete_part_non_existent.yaml'):
            with pytest.raises(ComponentDoesNotExist):
                self.partition.delete_part('partNameNonExistent')

    @pytest.mark.xfail(raises=AcosCreationError, strict=True)
    def test_delete_part_except(self):
        with vcr.use_cassette('vcr/recordings/partition/delete_part_except.yaml'):
            with patch.object(mockPartition, "delete", side_effect=acos_errors.ACOSException()):
                self.partition.delete_part("partName")


class TestDefaultRoute(unittest.TestCase):

    def setUp(self):
        self.client = ActionsClient(endpoint)
        with vcr.use_cassette('vcr/recordings/setup.yaml'):
            self.client.active_partition('partName')

        self.defaultRoute = DefaultRoute(self.client)

    def test_create_default_route_exists(self):
        with vcr.use_cassette('vcr/recordings/defaultroute/create_default_route_exists.yaml'):
            with pytest.raises(ComponentAlreadyExists):
                self.defaultRoute.create_default_route('0.0.0.0', '/0', '127.0.0.1')

    def test_create_default_route_creation_fail(self):
        with vcr.use_cassette('vcr/recordings/defaultroute/create_default_route_fail.yaml'):
            with pytest.raises(AcosCreationError):
                self.defaultRoute.create_default_route('00.0.0', '/0', '127.0.0.1')  # Incorrect ip triggers except

    def test_check_default_route_true(self):
        with vcr.use_cassette('vcr/recordings/defaultroute/check_default_route_true.yaml'):
            assert self.defaultRoute.check_default_route('0.0.0.0', '/0') is True

    def test_check_default_route_false(self):
        with vcr.use_cassette('vcr/recordings/defaultroute/check_default_route_false.yaml'):
            assert self.defaultRoute.check_default_route('1.1.1.1', '/24') is False

    @pytest.mark.xfail(raises=AcosCheckingError, strict=True)
    def test_check_default_route_except(self):
        with patch.object(mockRIB, "exists", side_effect=acos_errors.ACOSException()):
            self.defaultRoute.check_default_route('0.0.0.0', '/24')


class TestNatPool(unittest.TestCase):

    def setUp(self):
        self.client = ActionsClient(endpoint)
        with vcr.use_cassette('vcr/recordings/setup.yaml'):
            self.client.active_partition('partName')

        self.natPool = NatPool(self.client)

    def test_create_nat_pool_exists(self):
        with vcr.use_cassette('vcr/recordings/natpool/create_nat_pool_exists.yaml'):
            with pytest.raises(ComponentAlreadyExists):
                self.natPool.create_nat_pool('classPool', '3.3.3.3', '3.3.3.10', '/24', True, 2)

    def test_create_nat_pool_creation_fail(self):
        with vcr.use_cassette('vcr/recordings/natpool/create_nat_pool_fail.yaml'):
            with pytest.raises(AcosCreationError):
                self.natPool.create_nat_pool('classPoolNonExistent', '3333', '33310', '/2', True, 2)

    def test_check_nat_pool_true(self):
        with vcr.use_cassette('vcr/recordings/natpool/check_nat_pool_true.yaml'):
            assert self.natPool.check_nat_pool('classPool') is True

    def test_check_nat_pool_false(self):
        with vcr.use_cassette('vcr/recordings/natpool/check_nat_pool_false.yaml'):
            assert self.natPool.check_nat_pool('classPoolNonExistent') is False

    @pytest.mark.xfail(raises=AcosCheckingError, strict=True)
    def test_check_nat_pool_except(self):
        with patch.object(mockNat.Pool, "exists", side_effect=acos_errors.ACOSException()):
            self.natPool.check_nat_pool("classPoolNonExistent")


class TestVLAN(unittest.TestCase):

    def setUp(self):
        self.client = ActionsClient(endpoint)
        with vcr.use_cassette('vcr/recordings/setup.yaml'):
            self.client.active_partition('partName')

        self.vlan = VLAN(self.client)

    def test_create_vlan_exists(self):
        with vcr.use_cassette('vcr/recordings/vlan/create_vlan_exists.yaml'):
            with pytest.raises(ComponentAlreadyExists):
                self.vlan.create_vlan('vl1010', '1010', '1', '1010')

    def test_create_vlan_creation_fail(self):
        with vcr.use_cassette('vcr/recordings/vlan/create_vlan_fail.yaml'):
            with pytest.raises(AcosCreationError):
                self.vlan.create_vlan('vl10100', '10100', '1', '10100')

    def test_check_vlan_true(self):
        with vcr.use_cassette('vcr/recordings/vlan/check_vlan_true.yaml'):
            assert self.vlan.check_vlan('1010') is True  # 1010 exists

    def test_check_vlan_false(self):
        with vcr.use_cassette('vcr/recordings/vlan/check_vlan_false.yaml'):
            assert self.vlan.check_vlan('0000') is False  # 0000 doesn't exists

    @pytest.mark.xfail(raises=AcosCheckingError, strict=True)
    def test_check_vlan_except(self):
        with patch.object(mockVlan, "exists", side_effect=acos_errors.ACOSException()):
            self.vlan.check_vlan("0000")


class TestVRRP(unittest.TestCase):

    def setUp(self):
        self.client = ActionsClient(endpoint)
        with vcr.use_cassette('vcr/recordings/setup.yaml'):
            self.client.active_partition('partName')

        self.vrrp = VRRP(self.client)

    def test_create_vrrp_exists(self):
        with vcr.use_cassette('vcr/recordings/vrrp/create_vrrp_exists.yaml'):
            with pytest.raises(ComponentAlreadyExists):
                self.vrrp.create_vrid([1, 2])

    def test_create_vrrp_creation_fail(self):
        with vcr.use_cassette('vcr/recordings/vrrp/create_vrrp_fail.yaml'):
            with pytest.raises(AcosCreationError):
                self.vrrp.create_vrid([1000, 2000])

    def test_check_vrrp_true(self):
        with vcr.use_cassette('vcr/recordings/vrrp/check_vrrp_true.yaml'):
            assert self.vrrp.check_vrid(1) is True

    def test_check_vrrp_false(self):
        with vcr.use_cassette('vcr/recordings/vrrp/check_vrrp_false.yaml'):
            assert self.vrrp.check_vrid(123) is False

    @pytest.mark.xfail(raises=AcosCheckingError, strict=True)
    def test_check_vrrp_except(self):
        with patch.object(mockVRID, "exists", side_effect=acos_errors.ACOSException()):
            self.vrrp.check_vrid(1000)

    def test_configure_vrrp_non_existent(self):
        with vcr.use_cassette('vcr/recordings/vrrp/configure_vrrp_non_existent.yaml'):
            with pytest.raises(ComponentDoesNotExist):
                self.vrrp.configure_vrid([3, 4], 160)

    @pytest.mark.xfail(raises=AcosCreationError, strict=True)
    def test_configure_vrrp_except(self):
        with patch.object(mockBP, "create", side_effect=acos_errors.ACOSException()):
            self.vrrp.configure_vrid(['noInt', 'noInt'], 160)


class TestSSL(unittest.TestCase):

    def setUp(self):
        self.client = ActionsClient(endpoint)
        with vcr.use_cassette('vcr/recordings/setup.yaml'):
            self.client.active_partition('partName')

        self.ssl = SSL(self.client)

    def test_create_ssl_exists(self):
        with vcr.use_cassette('vcr/recordings/ssl/create_ssl_exists.yaml'):
            with pytest.raises(ComponentAlreadyExists):
                self.ssl.create_ssl_template('CipherNaam', 'FooBar')

    def test_create_ssl_creation_fail(self):
        with vcr.use_cassette('vcr/recordings/ssl/create_ssl_fail.yaml'):
            with pytest.raises(AcosCreationError):
                self.ssl.create_ssl_template('CipherNonExistent', 'Foobar')

    def test_check_ssl_true(self):
        with vcr.use_cassette('vcr/recordings/ssl/check_ssl_true.yaml'):
            assert self.ssl.check_ssl('CipherNaam') is True

    def test_check_ssl_false(self):
        with vcr.use_cassette('vcr/recordings/ssl/check_ssl_false.yaml'):
            assert self.ssl.check_ssl('CipherNonExistent') is False

    @pytest.mark.xfail(raises=AcosCheckingError, strict=True)
    def test_check_ssl_except(self):
        with patch.object(mockSSLCipher, "exists", side_effect=acos_errors.ACOSException()):
            self.ssl.check_ssl("CipherNonExistent")


class TestHTTP(unittest.TestCase):

    def setUp(self):
        self.client = ActionsClient(endpoint)
        with vcr.use_cassette('vcr/recordings/setup.yaml'):
            self.client.active_partition('partName')

        self.http = HTTP(self.client)

    def test_create_http_exists(self):
        with vcr.use_cassette('vcr/recordings/http/create_http_exists.yaml'):
            with pytest.raises(ComponentAlreadyExists):
                self.http.create_http_template('X-Forwarded-For', 1, '', 1)

    def test_create_http_creation_fail(self):
        with vcr.use_cassette('vcr/recordings/http/create_http_fail.yaml'):
            with pytest.raises(AcosCreationError):
                self.http.create_http_template('httpNonExistent', 'noInt', '', 'noInt')

    def test_check_http_true(self):
        with vcr.use_cassette('vcr/recordings/http/check_http_true.yaml'):
            assert self.http.check_http('X-Forwarded-For') is True

    def test_check_http_false(self):
        with vcr.use_cassette('vcr/recordings/http/check_http_false.yaml'):
            assert self.http.check_http('httpNonExistent') is False

    @pytest.mark.xfail(raises=AcosCheckingError, strict=True)
    def test_check_http_except(self):
        with patch.object(mockHTTPTemplate, "exists", side_effect=acos_errors.ACOSException()):
            self.http.check_http("httpNonExistent")


class TestPersistentCookie(unittest.TestCase):

    def setUp(self):
        self.client = ActionsClient(endpoint)
        with vcr.use_cassette('vcr/recordings/setup.yaml'):
            self.client.active_partition('partName')

        self.persistentCookie = PersistentCookie(self.client)

    def test_create_persistent_cookie_exists(self):
        with vcr.use_cassette('vcr/recordings/persistcookie/create_persistent_cookie_exists.yaml'):
            with pytest.raises(ComponentAlreadyExists):
                self.persistentCookie.create_persistent_cookie('CookiePersistence', None)

    @pytest.mark.xfail(raises=AcosCreationError, strict=True)  # xfail maybe for all creation_fail tests?
    def test_create_persistent_cookie_creation_fail(self):
        with vcr.use_cassette('vcr/recordings/persistcookie/create_persistent_cookie_fail.yaml'):
            with patch.object(mockCookiePersistence, "create", side_effect=acos_errors.ACOSException()):
                self.persistentCookie.create_persistent_cookie('CookieNonExistent', None)

    def test_check_persistent_cookie_true(self):
        with vcr.use_cassette('vcr/recordings/persistcookie/check_persistent_cookie_true.yaml'):
            assert self.persistentCookie.check_cookie('CookiePersistence') is True

    def test_check_persistent_cookie_false(self):
        with vcr.use_cassette('vcr/recordings/persistcookie/check_persistent_cookie_false.yaml'):
            assert self.persistentCookie.check_cookie('CookieNonExistent') is False

    @pytest.mark.xfail(raises=AcosCheckingError, strict=True)
    def test_check_persistent_cookie_except(self):
        with patch.object(mockCookiePersistence, "exists", side_effect=acos_errors.ACOSException()):
            self.persistentCookie.check_cookie("CookieNonExistent")
