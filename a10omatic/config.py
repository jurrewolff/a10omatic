from omniconf import setting, omniconf_load, help_requested, show_usage
from a10omatic.exceptions import YAMLLoadError


def string_nonetype(value):
    if value == 'null':
        return None
    return value


def load_config():
    """
    Method loads all required variables using omniconf.
    Loading can be done from yaml, environment vars, cli args and more.
    Throws exception when loading failed.
    Note: When a yaml file is specified other types of args are not utilised anymore.
    """

    try:
        # Config-Files
        setting('configfile.ssl.cfgpath', default='default',
                help='Used to set path for custom SSL-Template configuration file (yaml format). \
                Make sure to enter the full path.')
        setting('configfile.logger.cfgpath', _type=string_nonetype, default=None,
                help='Used to set path for custom logging configuration file (yaml format).')

        # Login
        setting('login.endpoint', required=True, help='Parameter contains all login information. \
                Format: https://username:password@host:port/')

        # Warning
        setting('warning.insecurerequest', default=False, _type=bool,
                help='Enable/Disable insecurerequest warnings (Boolean).')

        # CA-Certs
        setting('certificate.path', _type=string_nonetype, help='Path to certificate bundle')

        # On fail
        setting('onfail.deletepart', default=True, _type=bool,
                help='If creation fails halfway through, affected partition will be automatically removed.')

        # Partition
        setting('partition.name', required=True)
        setting('partition.apptype', required=True, default='adc',
                help='Used to set application type of partition. Defaults to type adc.')

        # Public Vlan
        setting('pubvlan.name', required=True, help='Used to set internal VLAN name.')
        setting('pubvlan.id', required=True, help='Used to set internal VLAN ID.')
        setting('pubvlan.ve.ip1', required=True, help='IP-Address for Virtual-Ethernet link in device context 1.')
        setting('pubvlan.ve.ip2', required=True, help='IP-Address for Virtual-Ethernet link in device context 2.')
        setting('pubvlan.ve.mask', required=True, help='Used to set Virtual-Ethernet network mask.')
        setting('pubvlan.taggedeths', _type=list, required=True,
                help='Used to set VLAN ethernet port(s). List can contain multiple entries.')

        # Internal Vlan
        setting('intvlan.name', required=True, help='Used to set internal VLAN name.')
        setting('intvlan.id', required=True, help='Used to set internal VLAN ID.')
        setting('intvlan.ve.ip1', required=True, help='IP-Address for Virtual-Ethernet link in device context 1.')
        setting('intvlan.ve.ip2', required=True, help='IP-Address for Virtual-Ethernet link in device context 2.')
        setting('intvlan.ve.mask', required=True, help='Used to set Virtual-Ethernet network mask.')
        setting('intvlan.taggedeths', _type=list, required=True,
                help='Used to set VLAN ethernet port(s). List can contain multiple entries.')

        # Default-Route
        setting('defaultroute.address', required=True, help='Used to set Default-Route address.')
        setting('defaultroute.mask', required=True, help='Used to set Default-Route mask.')
        setting('defaultroute.gateway', required=True, help='Used to set Default-Route gateway.')

        # Nat-Pool
        setting('natpool.name', required=True, help='Used to set NAT-Pool name.')
        setting('natpool.ipstart', required=True, help='Used to set NAT-Pool range-start IP-address.')
        setting('natpool.ipend', required=True, help='Used to set NAT-Pool range-end IP address.')
        setting('natpool.mask', required=True, help='Used to set NAT-Pool network mask.')
        setting('natpool.iprr', default=True, _type=bool, help='Enable/Disable rond-robin behaviour.')
        setting('natpool.vrid', _type=int, required=True, help='Configure VRRP-A VRID.')

        # VRRP
        setting('vrrp.vridlist', _type=list, required=True, help='List containing vrids to be created.')
        setting('vrrp.priority1', default=160, _type=int, help='Used to set priority in device-context 1.')
        setting('vrrp.priority2', default=200, _type=int, help='Used to set priority in device-context 2.')

        # ASCII
        setting('ascii', _type=bool, default=False, help='Print some nice ASCII.')

        # Display help message when requested
        if help_requested():
            show_usage()

        # Load settings
        omniconf_load(autoconfigure_prefix="")

    except Exception as ex:
        raise YAMLLoadError('Failed to load configuration: {}'.format(ex))
