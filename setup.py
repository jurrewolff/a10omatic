# Copyright (c) 2018 Jurre Wolff <jurre.wolff@cyso.com>
#
# This file is part of a10omatic .
#
# All rights reserved.

from setuptools import setup

setup(
    setup_requires=['pbr>=1.9', 'setuptools>=17.1'],
    pbr=True,
)
